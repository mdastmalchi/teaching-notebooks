'''
Simple laser diode simulator

Author: Jochen Schröder jochen.schroeder@chalmers.se

Licence: GPLv3

'''


from laser_object  import LaserDiode
import matplotlib.pyplot as plt
from ipywidgets import widgets
from IPython.display import display
from traitlets import link
from ipywidgets.widgets.interaction import interact

LD = LaserDiode()


thbutton = widgets.Button(description="Calculate Threshold")
th_text = widgets.Text(description="Threshold Current in mA", value = "0", disabled=True)

def threshold_plot():
    display(thbutton)
    display(th_text)

def on_thbutton_clicked(b):
    LD.cal_threshold(plot=True)
        #fig.show()
    th_text.value = "%.3f mA"%(LD.Ith*1e3)

thbutton.on_click(on_thbutton_clicked)

I0_text = widgets.BoundedFloatText(value=10,
                                        description="Zero point current in mA",
                                        max=80,
                                        min=0)
I1_text = widgets.BoundedFloatText(value=13,
                                        description="One point current in mA",
                                        max=100,
                                        min=0)
reprate_text = widgets.BoundedFloatText(value=1,
                                       description="Repetition rate in GHz",
                                       max=20,
                                       min=0.01)
dat_button = widgets.Button(description="Calculate data response")
N_slider = widgets.IntSlider(value=10,
                                 description="Number of Symbols",
                                 max=100)

def data_plot():
    display(N_slider)
    display(I0_text)
    display(I1_text)
    display(reprate_text)
    display(dat_button)


def on_data_click(b):
    I0 = I0_text.value*1e-3
    I1 = I1_text.value*1e-3
    rr = reprate_text.value*1e9
    N = N_slider.value
    LD.plot_data(I0, I1, reprate=rr, Npulse=N)

dat_button.on_click(on_data_click)


from __future__ import division, print_function
import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import odeint
import solve_rate_eqn

C = 2.99792458e8

def create_random_NRZOOK(t, reprate):
    rtmp = np.random.random_integers(0,1,
            size=int(np.ceil((t.max()-t.min())*reprate)))
    field = np.ones(t.size, dtype=np.float)
    for i in range(len(rtmp)):
        #field[np.where((abs(abs(t)-(i+1)/reprate)<=1)&(abs(t)-
            #(i+1)/reprate>0))] *=rtmp[i]
        idx = np.where(abs(t*reprate-i)<=0.5)
        field[idx] *= rtmp[i]
    return field

def rk4(fct, y, t, dt):
    k1 = fct(y, t)
    k2 = fct(y+0.5*k1*dt, t+0.5*dt)
    k3 = fct(y+0.5*k2*dt, t+0.5*dt)
    k4 = fct(y+k3*dt, t+dt)
    return y + 1/6.*(k1+2*k2+2*k3+k4)*dt

class LaserDiode(object):
    gamma = 0.44
    g0 = 3.e-6
    N0 = 1.2e18
    eps = 3.4e-17
    tau_p = 1.e-12
    beta = 4.e-4
    #tau_n = 1.e-9
    tau_n = 3.e-9
    V = 9.e-11
    q = 1.602e-19
    eta = 0.1
    hbar=6.624e-34
    alpha=0.
    Ne = 5.41e10
    f0 = C/1550.e-9

    def get_wl(self):
        return C/self.f0

    def set_wl(self, wl):
        self.f0 = C/wl
    wl = property(get_wl, set_wl)

    def set_Q(self, Q):
        self.tau_p = Q/(2*np.pi*self.f0)

    def get_Q(self):
        return self.f0*self.tau_p*2*np.pi
    Q = property(get_Q, set_Q)

    def _rateequations(self, state, time):
        #based on L. Bjerkan in JLT 14 N 5, 839 (1996)
        fract = self.g0*(state[0]-self.N0)*state[1]/(1+self.eps*state[1])
        N = state[3]/(self.q*self.V)-fract-state[0]/self.tau_n
        S = self.gamma*fract-state[1]/self.tau_p+self.gamma*self.beta/self.tau_n*state[0]
        phi = 0.5*self.alpha*(self.gamma*self.g0*(state[0]-self.N0)-1/self.tau_p)
        return np.array([N, S, phi, state[3]])


    def cal_threshold(self, tmax=1.e-7, dt=1.e-12, plot=False):
        I = np.linspace(0, 40, 200)*1e-3
        t = np.arange(0, tmax, dt)
        answer = solve_rate_eqn.cal_threshold(I, t, dt, self.g0, self.N0, self.eps, self.V, self.tau_n, self.gamma, self.beta, self.alpha, self.tau_p)
        pout = self._cal_P_from_S(answer[1])
        idx = np.where(pout>pout.mean())
        p = np.polyfit(I[idx], pout[idx], 1)
        self.Ith = -p[1]/p[0]
        if plot:
            fig = plt.figure()
            ax = plt.subplot(111)
            ax.set_title("P-I curve")
            ax.plot(I*1e3, pout)
            ax.plot(I*1e3, I*p[0]+p[1], 'r')
            ax.axis([0, I.max()*1e3, 0, pout.max()])
            ax.annotate(r"$I_{th}=%.2f\ [mA]$"%(self.Ith*1e3), xy=(self.Ith*1e3, 0),
                        xytext=(self.Ith*1e3, 0.1),
                        arrowprops=dict(facecolor='black', shrink=0.2),)
            ax.set_ylabel("P [mW]")
            ax.set_xlabel("I [mA]")
            #plt.show()
            return fig
        else:
            return I, p, pout

    def _cal_P_from_S(self, S):
        return S*self.V*self.eta*self.hbar*self.f0/(2*self.gamma*self.tau_p)*1e3

    def plot_data(self, Ith, Imod, reprate=5.e8, Npulse=20, dt=0.5e-12):
        t = np.arange(0, Npulse/reprate, dt)
        data = create_random_NRZOOK(t, reprate)
        I = data*Imod+Ith
        answer = solve_rate_eqn.cal_data(I, t, dt, self.g0, self.N0, self.eps, self.V, self.tau_n, self.gamma, self.beta, self.alpha, self.tau_p)
        pout = self._cal_P_from_S(answer[:,1])
        if hasattr(self, "data_fig"):
            self.p1.set_xdata(t*1e9)
            self.p1.set_ydata(pout)
            self.p2.set_xdata(t*1e9)
            self.p2.set_ydata(data)
            self.ax1.set_xbound((t.min()*1e9, t.max()*1e9))
            self.ax1.set_ybound((pout.min(), pout.max()))
        else:
            self.data_fig = plt.figure()
            self.ax1 = self.data_fig.add_subplot(111)
            self.p1, = self.ax1.plot(t*1e9, pout, label='output')
            self.p2, = self.ax1.plot(t*1e9, data, label='data')
            self.ax1.legend()
            self.ax1.set_xlabel("t [ns]")
            self.ax1.set_ylabel("P [mW]")
            #plt.show()
            #return self.data_fig


if __name__ == "__main__":
    LD = LaserDiode()
    #LD.cal_threshold(plot=True)
    #LD.plot_data(10e-3, 10.e-3, Npulse=40)








